# weather.sh

Simple bash script to display the weather using OpenWeatherMap. In order for this script to work, you need to sign up for a free API key with OpenWeatherMap, available [here](https://openweathermap.org/home/sign_up). `jq` is also required for processing the JSON files.

This script is basically just a way for me to learn bash scripting, so apologies for the mess. It is licensed under the MIT license.

## Options

- **-a** Enables all display options. This is the default option.
- **-c** Shows the city requested.
- **-C** Shows current weather conditions.
- **-f** Shows what the temperature feels like.
- **-t** Shows the actual temperature.
- **-u** Changes temperature units. Options are Celsius (default), Fahrenheit, and Kelvin.
