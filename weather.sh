#!/bin/bash

# Change path to weather.conf here.
. weather.conf

if [ $# -gt 0 ]
then
    if [ "$units" == "metric" ]
    then
        degree="\u00B0C"
    elif [ "$units" == "imperial" ]
    then
        degree="\u00B0F"
    else
        degree=" K"
    fi

    while [ -n "$1" ]; do
        case "$1" in
            -a)
                enableCity=true; enableTemp=true; enableFeel=true; enableCond=true ;;
            -c)
                enableCity=true ;;
            -C)
                enableCond=true ;;
            -f)
                enableFeel=true ;;
            -t)
                enableTemp=true ;;
            -u)
                param="$2"
                case "$2" in
                    "c" | "celsius" | "metric") units="metric"; degree="\u00B0C" ;;
                    "k" | "kelvin" | "scientific") units=""; degree=" K" ;;
                    "f" | "fahrenheit" | "imperial") units="imperial"; degree="\u00B0F" ;;
                esac
        esac
        shift
    done
else
    if [ "$units" == "metric" ]
    then
        degree="\u00B0C"
    elif [ "$units" == "imperial" ]
    then
        degree="\u00B0F"
    else
        degree=" K"
    fi
    enableCity=true
    enableTemp=true
    enableFeel=true
    enableCond=true
fi

grabAPI() {
    if [ -z "$units" ]
    then
        curl -s "http://api.openweathermap.org/data/2.5/weather?id=${CITY_ID}&APPID=${API_KEY}" > /tmp/localweather
    else
        curl -s "http://api.openweathermap.org/data/2.5/weather?id=${CITY_ID}&APPID=${API_KEY}&units=${units}" > /tmp/localweather
    fi
}

jsonProcessing() {
    city=`jq -r '.name' /tmp/localweather`
    country=`jq -r '.sys.country' /tmp/localweather`
    temp=`jq -r '.main.temp' /tmp/localweather`
    feel=`jq -r '.main.feels_like' /tmp/localweather`
    conditionsRaw=`jq -r '.weather[].id' /tmp/localweather`
}

conditionProcessing() {
    case $conditionsRaw in
        2[[:digit:]]) cond="Thunderstorm" ;;
        3[[:digit:]]) cond="Drizzling" ;;
        5[[:digit:]]) cond="Raining" ;;
        6[[:digit:]]) cond="Snowing" ;;
        800 | 801) cond="Clear skies" ;;
        802) cond="Partly cloudy" ;;
        803) cond="Mostly cloudy" ;;
        804) cond="Overcast" ;;
    esac
}

outputWeather() {
    if [ $enableCity ]
    then
        echo "City: ${city}, ${country}"
    fi
    if [ $enableTemp ]
    then
        echo -e "Current temperature is ${temp}${degree}"
    fi
    if [ $enableFeel ]
    then
        echo -e "It feels like ${feel}${degree}"
    fi
    if [ $enableCond ]
    then
        echo -e "$cond"
    fi
}

grabAPI
jsonProcessing
conditionProcessing
outputWeather

rm /tmp/localweather
